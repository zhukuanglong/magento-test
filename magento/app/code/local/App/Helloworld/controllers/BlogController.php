<?php
class App_Helloworld_BlogController extends Mage_Core_Controller_Front_Action { 
    
    //test EAV
    public function eavReadAction(){
        $eavModel = Mage::getModel('helloworld/eavblogpost');
        $params = $this->getRequest()->getParams();
        $eavModel->load($params['id']);
        $data = $eavModel->getData();
        var_dump($data);
    }

    //add eav data
    public function addEavDataAction(){
        for($i=10;$i<20;$i++){
            $eavmodel = Mage::getModel('helloworld/eavblogpost');
            $eavmodel->setTitle('eav blog test :'.$i);
            $eavmodel->setPost('some content here :'.$i);
            $eavmodel->save();
        }
        echo 'done';
    }

    //show all eav data
    public function showEavDataAction(){
        $eavmodel = Mage::getModel('helloworld/eavblogpost');
        $allposts = $eavmodel->getCollection()->addAttributeToSelect(array('title','post'));
        $allposts->load();
        foreach ($allposts as $post) {
            echo '<h1>'.$post->getTitle().'</h1>';
            echo '<br>'.$post->getPost().'<br>';
        }
    }


    //blog id  read
    public function indexAction()          
    { 
        $params = $this->getRequest()->getParams(); 
	    $blogpost = Mage::getModel('helloworld/blogpost'); 
	    $blogpost->load($params['id']);      
	    $data = $blogpost->getData(); 
	    var_dump($data); 
    }


    //add blog
    public function addBlogAction(){
    	$blog = Mage::getModel('helloworld/blogpost');
    	$blog->setTitle('add a new blog');
        $timenow = date('Y-m-d H:i:s',time());
    	$blog->setPost('add blog now,hahah,time is'.$timenow);
    	$blog->save();
    } 

    //blog list  模型集合
    public function showAllBlogPostsAction(){
    	$posts = Mage::getModel('helloworld/blogpost')->getCollection();
    	foreach($posts as $post){
    		echo '<h3>'.$post->getData('title').'</h3>'."<br>";
    		echo nl2br($post->getData('post')).'<br>';
    	}
    }
}
