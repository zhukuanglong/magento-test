<?php
echo 'Testing our upgrade script (mysql4-upgrade-0.1.0-0.2.0.php) and halting execution to avoid updating the system version number  '; 
$installer = $this;
$installer->startSetup(); 
$installer->run("alter table `{$installer->getTable('helloworld/blogpost')}` change post  post text not null");
$installer->endSetup();
