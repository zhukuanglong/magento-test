<?php
class App_Helloworld_Model_Resource_Eav_Mysql4_Blogpost extends Mage_Eav_Model_Entity_Abstract{
	
	public function _construct(){
		$resource = Mage::getSingleton('core/resource');
		$this->setType('helloworld_eavblogpost');
		$this->setConnection(
			$resource->getConnection('helloworldeav_write'),
			$resource->getConnection('helloworldeav_read')
			);
	}


}
